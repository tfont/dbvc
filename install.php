<h2>Database Configurations</h2>
<form action="install.php" method="post">
    Host/Server: <input type="text" name="host" id="host" value="localhost"><br>
    Database: <input type="text" name="database" id="database" value="dbvc"><br>
    User: <input type="text" name="user" id="user"><br>
    Password: <input type="text" name="password" id="password"><br>
    <input type="submit" value="Begin Initial Setup!">
</form>

<?php

require_once 'functions.php';

if ($_POST)
{
    if (
        (isset($_POST['host'])     && !empty($_POST['host']))     &&
        (isset($_POST['database']) && !empty($_POST['database'])) &&
        (isset($_POST['user'])     && !empty($_POST['user']))     &&
        (isset($_POST['password']))
       )
    {

        // checking writing permission
        @chmod('.', 0777);
        if (!is_writable('.'))
        {
            throw new Exception('You must have write capabilities for the directory where DBVC is installed.');
        }
        @chmod('configs.php', 0777);
        if (!is_writable('configs.php'))
        {
            throw new Exception('could not write to configs.php in the main DBVC directory.');
        }

        // read and extract config file (per line)
        $config_file = explode(PHP_EOL, file_get_contents('configs.php'));

        foreach ($config_file as $line => $file)
        {

            if (strpos($file, 'define') !== FALSE)
            {
                // database host config
                if (strpos($file, "define('DB_HOST',") !== FALSE)
                {
                    $config_file[$line] = "define('DB_HOST', '".$_POST['host']."');";
                }

                // database name config
                if (strpos($file, "define('DB_NAME',") !== FALSE)
                {
                    $config_file[$line] = "define('DB_NAME', '".$_POST['database']."');";
                }

                // database user config
                if (strpos($file, "define('DB_USER',") !== FALSE)
                {
                    $config_file[$line] = "define('DB_USER', '".$_POST['user']."');";
                }

                // database password config
                if (strpos($file, "define('DB_PASSWORD',") !== FALSE)
                {
                    $config_file[$line] = "define('DB_PASSWORD', '".$_POST['password']."');";
                }

            }
        }

        // stack and overwrite to config file
        $file_content = implode(PHP_EOL, $config_file);
        if (file_put_contents('configs.php', $file_content))
        {
            echo "\n<p>Config file updated.</p>\n";

            try
            {
                $db = new PDO("mysql:host=".$_POST['host'].";charset=utf8", $_POST['user'], $_POST['password']);
                //$db = new PDO("mysql:host=".$_POST['host'].";dbname=".$_POST['database'].";charset=utf8", $_POST['user'], $_POST['password']);
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                // creating the config database (if not exiting)
                $db->query("CREATE DATABASE IF NOT EXISTS ".$_POST['database']." CHARACTER SET utf8 COLLATE utf8_general_ci;");

                // creating settings table
                if(file_exists('sql/dbvc.sql'))
                {
                    try
                    {
                        if (
                            $db->query(str_replace('%#DB#%', $_POST['database'], file_get_contents('sql/dbvc.sql')))
                            &&
                            $db->query(str_replace('%#DB#%', $_POST['database'], file_get_contents('sql/databases.sql')))
                           )
                        {
                            // once completed - redirects to main page
                            redirectBase();
                        }
                    }
                    catch (PDOException $e)
                    {
                        die("\n<p><strong>Error:</strong> ".$e->getMessage()."</p>");
                    }
                }
                else
                {
                    die('dbvc.sql file does not exist. Cannot continue!');
                }

            }
            catch (PDOException $e)
            {
                die("\n<p><strong>Error:</strong> ".$e->getMessage()."</p>");
            }

        }
        else
        {
            die('Error writing to config file! Try again.');
        }

    }
    else
    {
        die('All Database configuration fields are required.');
    }
}

?>
<?php

// Report all errors
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

require_once 'configs.php';
require_once 'functions.php';
require_once 'database.class.php';
require_once 'backup_database.class.php';


$db = new Database(DB_HOST, DB_USER, DB_PASSWORD);

// trigger actions
if ($_GET)
{
    if (isset($_GET['db']) && isset($_GET['action']) && $_GET['action'] == 'on')
    {
        // turns on (adds)
        $db->turnOn($_GET['db']);
    }
    if (isset($_GET['db']) && isset($_GET['action']) && $_GET['action'] == 'off')
    {
        // turns off (deletes)
        $db->turnOff($_GET['db']);
    }
}

$databases = $db->showDatabases();
if (isset($databases) && count($databases) > 0)
{
    echo "<h3>Databases on ".DB_HOST.":</h3>";

    $active_databases = $db->getActiveDatabases();
    foreach ($databases as $item)
    {
        echo "<strong>".$item->Database."</strong> | [<a href=\"?db=".$item->Database."&action=";
        if (in_array($item->Database, $active_databases))
        {
            echo "off\">ON</a>] | [<a href=\"?update=".$item->Database."&v=".$db->getLatestVersion()."\">update</a>]<br/>";
        }
        else
        {
            echo "on\">OFF</a>]<br/>";
        }

    }
}


echo "<h3><a href=\"?revision\">Push update</a> (current: ".$db->getLatestVersion().")</h3>";

// revision dump magic
if (key($_GET) == 'revision')
{
    echo "<hr>";

    #checkDumpFolder();
    // creating dump directory if not exist (first time run)
    if (!file_exists('./dumps'))
    {
        mkdir('./dumps', 0777, TRUE);
    }

    // retrieving dump directory info
    $directory   = list_directory('./dumps/');
    $last_folder = end($directory);

    $folder = explode('./dumps/', $last_folder);

    // no folders exist
    if (!isset($folder[1]))
    {
        $num = '0';
    }
    else
    {
        $num = $folder[1];
    }

    // checking version and adding new one version
    $num = str_replace('e', '', $num);
    $num = $num*1;
    $new_verion_num = leading_zero($num+1, 11);

    // creates the new version folder
    if (!file_exists('./dumps/e'.$new_verion_num))
    {
        // inserts the current revision number
        $db->updateVersion('e'.$new_verion_num);

        // creates the revision dump folder
        mkdir('./dumps/e'.$new_verion_num, 0777, TRUE);
    }

    // backup process
    foreach ($active_databases as $database)
    {
        $backupDatabase = new Backup_Database(DB_HOST, DB_USER, DB_PASSWORD, $database);
        $status = $backupDatabase->backupTables('*', 'dumps/e'.$new_verion_num) ? 'OK' : 'ERROR';
    }

    if (isset($status))
    {
        // backup report
        echo "<br /><br /><br />Backup result: ".$status;
    }
}

if ($_GET && isset($_GET['update']) && isset($_GET['v']))
{
    $db->updateDatabase($_GET['update'], $_GET['v']);
}
<?php

function leading_zero($num, $places = 0)
{
    return str_pad($num, $places, '0', STR_PAD_LEFT);
}

function list_directory($directory, $recursive = FALSE)
{
    $array_items = array();
    if ($handle = opendir($directory))
    {
        while (FALSE !== ($file = readdir($handle)))
        {
            if ($file != "." && $file != "..")
            {
                if (is_dir($directory. "/" . $file))
                {
                    if($recursive)
                    {
                        $array_items = array_merge($array_items, list_directory($directory. "/" . $file, $recursive));
                    }

                    $file = $directory . "/" . $file;
                    $array_items[] = preg_replace("/\/\//si", "/", $file);
                }
                else
                {
                    $file = $directory . "/" . $file;
                    $array_items[] = preg_replace("/\/\//si", "/", $file);
                }
            }
        }

        closedir($handle);
    }

    return $array_items;
}

function redirectBase()
{
    header('Location: http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/');
}

function redirectInstall()
{
    header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'install.php');
}
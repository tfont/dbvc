<?php

define ('DATA_CHUNK_LENGTH', 1638400);
define ('TESTMODE',false); // NOTE: replace all these so this constant is no longer needed

@ini_set('max_execution_time', 0);
@ini_set('auto_detect_line_endings', TRUE);
ini_set('mysql.connect_timeout', 14400*60*60);
ini_set('default_socket_timeout', 14400*60*60);
ini_set('default_socket_timeout', 14400*60*60);
ini_set('memory_limit','512M');

require_once 'configs.php';
require_once 'functions.php';

class Database
{
    protected $host;
    protected $user;
    protected $password;
    protected $charset;
    protected $db;

    public function __construct($host, $user, $password, $charset = 'utf8')
    {
        $this->host     = $host;
        $this->user     = $user;
        $this->password = $password;
        $this->charset  = $charset;

        $this->initializeDatabase();
    }

    protected function initializeDatabase()
    {
        $this->db = new PDO("mysql:host=$this->host;charset=$this->charset", $this->user, $this->password);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function showDatabases()
    {
        try
        {
            $query = $this->db->prepare("SHOW DATABASES");
            if ($query->execute())
            {
                return $query->fetchAll(PDO::FETCH_CLASS);
            }
            else
            {
                return FALSE;
            }
        }
        catch (PDOException $e)
        {
            die("\n<p><strong>Error - ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
        }
    }

    public function getActiveDatabases()
    {
        try
        {
            $query = $this->db->prepare("SELECT * FROM  `".DB_NAME."`.`databases` ");
            if ($query->execute())
            {
                $databases = $query->fetchAll(PDO::FETCH_CLASS);
                $db_array  = array();
                foreach ($databases as $database)
                {
                    $db_array[] = $database->name;
                }

                if (count($db_array) > 0)
                {
                    return $db_array;
                }
                else
                {
                    return [];
                }
            }
            else
            {
                return [];
            }
        }
        catch (PDOException $e)
        {
            if($e->errorInfo[0] == '42S02')
            {
                // config file database doesn't exit
                redirectInstall();
            }
            else
            {
                die("\n<p><strong>Error - ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
            }
        }
    }

    public function turnOn($database)
    {
        $values = array($database);
        try
        {
            $query = $this->db->prepare("INSERT INTO `".DB_NAME."`.`databases` (`id` ,`name`)VALUES (NULL, ?);");
            if ($query->execute($values))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        catch (PDOException $e)
        {
            die("\n<p><strong>Error- ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
        }
    }

    public function turnOff($database)
    {
        $values = array($database);
        try
        {
            $query = $this->db->prepare("DELETE FROM `".DB_NAME."`.`databases` WHERE `databases`.`name` = ?");
            if ($query->execute($values))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        catch (PDOException $e)
        {
            die("\n<p><strong>Error - ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
        }
    }

    public function updateDatabase($database, $version)
    {
        $directory = './dumps/'.$version;
        if (file_exists($directory))
        {
            // note: reverse the array to table dumps come before data dumps
            $version_directory = array_reverse(list_directory($directory));
            $db_char = strlen($database);
            foreach($version_directory as $file)
            {
                if (substr(str_replace($directory.'/', '', $file), 0, $db_char) == $database)
                {
                    $file = str_replace('./', '', $file);




















                    $error = FALSE;

                    $_REQUEST["start"]        = 1;
                    $_REQUEST["foffset"]      = 0;
                    $_REQUEST["totalqueries"] = 0;
                    $_REQUEST["delimiter"]    = ';';
                    $_REQUEST["fn"]    = 'tb-logs-DATA.sql';
                    $curfilename       = 'tb-logs-DATA.sql';

                    $linespersession   = 3000;
                    $max_query_lines   = 300;
                    $csv_insert_table   = '';     // Destination table for CSV files
                    #$csv_preempty_table = false;  // true: delete all entries from table specified in $csv_insert_table before processing
                    $csv_delimiter      = ',';    // Field delimiter in CSV file
                    $csv_add_quotes     = true;   // If your CSV data already have quotes around each field set it to false
                    $csv_add_slashes    = true;   // If your CSV data already have slashes in front of ' and " set it to false
                    $string_quotes      = '\'';   // Change to '"' if your dump file uses double qoutes for strings
                    $dbconnection = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);


                    $file = fopen($directory.'/'.$curfilename, "r");
                    $filesize = ftell($file);

                    $_REQUEST["start"]   = floor($_REQUEST["start"]);
                    $_REQUEST["foffset"] = floor($_REQUEST["foffset"]);

                    $delimiter = $_REQUEST["delimiter"];

                    $comment[] ='#';                       // Standard comment lines are dropped by default
                    $comment[] ='-- ';
                    $comment[] ='DELIMITER';               // Ignore DELIMITER switch as it's not a valid SQL statement
                    // $comment[]='---';                  // Uncomment this line if using proprietary dump created by outdated mysqldump
                    // $comment[]='CREATE DATABASE';      // Uncomment this line if your dump contains create database queries in order to ignore them
                    $comment[] ='/*!';                     // Or add your own string to leave out other proprietary things


                    // Empty CSV table if requested
                    /*
                    if (!$error && $_REQUEST["start"] == 1 && $csv_insert_table != "" && $csv_preempty_table)
                    {
                        $query = "DELETE FROM `$csv_insert_table`";
                        if (!TESTMODE && !mysql_query(trim($query), $dbconnection))
                        {
                            echo ("<p class=\"error\">Error when deleting entries from $csv_insert_table.</p>\n");
                            echo ("<p>Query: ".trim(nl2br(htmlentities($query)))."</p>\n");
                            echo ("<p>MySQL: ".mysql_error()."</p>\n");
                            $error = true;
                        }
                    }
                    */
                    /*
                    // Print start message
                    if (preg_match("/\.gz$/i", $curfilename))
                    {
                        $gzipmode=true;
                    }

                    else
                    {
                        $gzipmode=false;
                    }

                    // Check $_REQUEST["foffset"] upon $filesize (can't do it on gzipped files)
                    if (!$error && !$gzipmode && $_REQUEST["foffset"] > $filesize)
                    {
                        echo ("<p class=\"error\">UNEXPECTED: Can't set file pointer behind the end of file</p>\n");
                        $error=true;
                    }
                    */

                    // Set file pointer to $_REQUEST["foffset"]

                    /*
                    if (!$error && ((!$gzipmode && fseek($file, $_REQUEST["foffset"])!=0) || ($gzipmode && gzseek($file, $_REQUEST["foffset"])!=0)))
                    { echo ("<p class=\"error\">UNEXPECTED: Can't set file pointer to offset: ".$_REQUEST["foffset"]."</p>\n");
                        $error=true;
                    }
                    */

                    // Start processing queries from $file

                    if (!$error)
                    {
                        $query        = "";
                        $queries      = 0;
                        $totalqueries = $_REQUEST["totalqueries"];
                        $linenumber   = $_REQUEST["start"];
                        $querylines   = 0;
                        $inparents    = false;

                        // Stay processing as long as the $linespersession is not reached or the query is still incomplete
                        while ($linenumber < $_REQUEST["start"]+$linespersession || $query!="")
                        {

                            // Read the whole next line

                            $dumpline = "";
                            while (!feof($file) && substr($dumpline, -1) != "\n" && substr($dumpline, -1) != "\r")
                            {
                                $dumpline .= fgets($file, DATA_CHUNK_LENGTH);
                                /*
                                if (!$gzipmode)
                                {
                                    $dumpline .= fgets($file, DATA_CHUNK_LENGTH);
                                }
                                else
                                {
                                    $dumpline .= gzgets($file, DATA_CHUNK_LENGTH);
                                }
                                */
                            }

                            if ($dumpline === "")
                            {
                                break;
                            }

                            // Remove UTF8 Byte Order Mark at the file beginning if any

                            if ($_REQUEST["foffset"] == 0)
                            {
                                $dumpline = preg_replace('|^\xEF\xBB\xBF|','', $dumpline);
                            }

                            // Create an SQL query from CSV line

                            if (($csv_insert_table != "") && (preg_match("/(\.csv)$/i", $curfilename)))
                            {
                                if ($csv_add_slashes)
                                {
                                    $dumpline = addslashes($dumpline);
                                }
                                $dumpline = explode($csv_delimiter, $dumpline);

                                if ($csv_add_quotes)
                                {
                                    $dumpline = "'".implode("','", $dumpline)."'";
                                }
                                else
                                {
                                    $dumpline = implode(",", $dumpline);
                                }
                                $dumpline = 'INSERT INTO '.$csv_insert_table.' VALUES ('.$dumpline.');';
                            }

                            // Handle DOS and Mac encoded linebreaks (I don't know if it really works on Win32 or Mac Servers)

                            $dumpline = str_replace("\r\n", "\n", $dumpline);
                            $dumpline = str_replace("\r", "\n", $dumpline);

                            // DIAGNOSTIC
                            // echo ("<p>Line $linenumber: $dumpline</p>\n");

                            // Recognize delimiter statement

                            if (!$inparents && strpos($dumpline, "DELIMITER ") === 0)
                            {
                                $delimiter = str_replace("DELIMITER ","", trim($dumpline));
                            }

                            // Skip comments and blank lines only if NOT in parents

                            if (!$inparents)
                            {
                                $skipline = false;
                                reset($comment);
                                foreach ($comment as $comment_value)
                                {

                                    // DIAGNOSTIC
                                    //          echo ($comment_value);
                                    if (trim($dumpline) == "" || strpos(trim($dumpline), $comment_value) === 0)
                                    {
                                        $skipline = true;
                                        break;
                                    }
                                }
                                if ($skipline)
                                {
                                    $linenumber++;

                                    // DIAGNOSTIC
                                    // echo ("<p>Comment line skipped</p>\n");

                                    continue;
                                }
                            }

                            // Remove double back-slashes from the dumpline prior to count the quotes ('\\' can only be within strings)

                            $dumpline_deslashed = str_replace("\\\\", "", $dumpline);

                            // Count ' and \' (or " and \") in the dumpline to avoid query break within a text field ending by $delimiter

                            $parents = substr_count($dumpline_deslashed, $string_quotes)-substr_count($dumpline_deslashed, "\\$string_quotes");
                            if ($parents % 2 != 0)
                            {
                                $inparents=!$inparents;
                            }

                            // Add the line to query

                            $query .= $dumpline;

                                // Don't count the line if in parents (text fields may include unlimited linebreaks)

                            if (!$inparents)
                            {
                                $querylines++;
                            }

                            // Stop if query contains more lines as defined by $max_query_lines

                            if ($querylines > $max_query_lines)
                            {
                                echo ("<p class=\"error\">Stopped at the line $linenumber. </p>");
                                echo ("<p>At this place the current query includes more than ".$max_query_lines." dump lines. That can happen if your dump file was ");
                                echo ("created by some tool which doesn't place a semicolon followed by a linebreak at the end of each query, or if your dump contains ");
                                echo ("extended inserts or very long procedure definitions. Please read the <a href=\"http://www.ozerov.de/bigdump/usage/\">BigDump usage notes</a> ");
                                echo ("for more infos. Ask for our support services ");
                                echo ("in order to handle dump files containing extended inserts.</p>\n");
                                $error = true;
                                break;
                            }

                            // Execute query if end of query detected ($delimiter as last character) AND NOT in parents

                            // DIAGNOSTIC
                            // echo ("<p>Regex: ".'/'.preg_quote($delimiter).'$/'."</p>\n");
                            // echo ("<p>In Parents: ".($inparents?"true":"false")."</p>\n");
                            // echo ("<p>Line: $dumpline</p>\n");

                            if ((preg_match('/'.preg_quote($delimiter, '/').'$/', trim($dumpline)) || $delimiter =='') && !$inparents)
                            {

                                // Cut off delimiter of the end of the query

                                $query = substr(trim($query), 0, -1*strlen($delimiter));

                                // DIAGNOSTIC
                                #echo ("<p>Query: ".trim(nl2br(htmlentities($query)))."</p>\n");
                                $db = mysql_select_db('tb');

                                if (!TESTMODE && !mysql_query($query, $dbconnection))
                                {
                                    echo ("<p class=\"error\">Error at the line $linenumber: ". trim($dumpline)."</p>\n");
                                    echo ("<p>Query: ".trim(nl2br(htmlentities($query)))."</p>\n");
                                    echo ("<p>MySQL: ".mysql_error()."</p>\n");
                                    $error = true;
                                    break;
                                }

                                $totalqueries++;
                                $queries++;
                                $query      = "";
                                $querylines = 0;
                            }
                            $linenumber++;
                        }
                    }

                    // Get the current file position
                    /*
                    if (!$error)
                    {
                        if (!$gzipmode)
                        {
                            $foffset = ftell($file);
                        }
                    }
                    else
                    {
                        $foffset = gztell($file);
                    }

                    if (!$foffset)
                    { echo ("<p class=\"error\">UNEXPECTED: Can't read the file pointer offset</p>\n");
                        $error=true;
                    }
                   */

                    // Print statistics

                    //skin_open();

                    // echo ("<p class=\"centr\"><b>Statistics</b></p>\n");

                    $foffset = ftell($file);

                    if (!$error)
                    {
                        //$lines_this   = $linenumber-$_REQUEST["start"];
                        $lines_done   = $linenumber-1;
                        //$lines_togo   = ' ? ';
                        //$lines_tota   = ' ? ';

                        //$queries_this = $queries;
                        $queries_done = $totalqueries;
                        //$queries_togo = ' ? ';
                        //$queries_tota = ' ? ';

                        //$bytes_this   = $foffset-$_REQUEST["foffset"];
                        $bytes_done   = $foffset;
                        //$kbytes_this  = round($bytes_this/1024,2);
                        $kbytes_done  = round($bytes_done/1024,2);
                        //$mbytes_this  = round($kbytes_this/1024,2);
                        $mbytes_done  = round($kbytes_done/1024,2);

                        //if (!$gzipmode)
                        //{
                            //$bytes_togo  = $filesize-$foffset;
                        //$bytes_tota  = $filesize;
                            //$kbytes_togo = round($bytes_togo/1024,2);
                        //$kbytes_tota = round($bytes_tota/1024,2);
                            //$mbytes_togo = round($kbytes_togo/1024,2);
                        //$mbytes_tota = round($kbytes_tota/1024,2);

                            //$pct_this   = ceil($bytes_this/$filesize*100);
                            //$pct_done   = ceil($foffset/$filesize*100);
                            //$pct_togo   = 100 - $pct_done;
                            //$pct_tota   = 100;
/*
                            if ($bytes_togo == 0)
                            {
                                $lines_togo   = '0';
                                $lines_tota   = $linenumber-1;
                                $queries_togo = '0';
                                $queries_tota = $totalqueries;
                            }
                        */

                           // $pct_bar    = "<div style=\"height:15px;width:$pct_done%;background-color:#000080;margin:0px;\"></div>";
                            /*
                        }
                        else
                        {
                            $bytes_togo  = ' ? ';
                            $bytes_tota  = ' ? ';
                            $kbytes_togo = ' ? ';
                            $kbytes_tota = ' ? ';
                            $mbytes_togo = ' ? ';
                            $mbytes_tota = ' ? ';

                            $pct_this    = ' ? ';
                            $pct_done    = ' ? ';
                            $pct_togo    = ' ? ';
                            $pct_tota    = 100;
                            $pct_bar     = str_replace(' ','&nbsp;','<tt>[         Not available for gzipped files          ]</tt>');
                        }
                        */

                        echo ("
                                <center>
                                <table width=\"520\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\">
                                <tr><th class=\"bg4\"> </th><th class=\"bg4\">Done</th></tr>
                                <tr><th class=\"bg4\">Lines</th><td class=\"bg3\">$lines_done</td></tr>
                                <tr><th class=\"bg4\">Queries</th><td class=\"bg3\">$queries_done</td></tr>
                                <tr><th class=\"bg4\">Bytes</th><td class=\"bg3\">$bytes_done</td></tr>
                                <tr><th class=\"bg4\">KB</th><td class=\"bg3\">$kbytes_done</td></tr>
                                <tr><th class=\"bg4\">MB</th><td class=\"bg3\">$mbytes_done</td></tr>

                                </table>
                                </center>
                                \n");

                        // Finish message and restart the script
                        if ($linenumber < $_REQUEST["start"]+$linespersession)
                        {
                            echo ("<p class=\"successcentr\">Congratulations: End of file reached, assuming OK</p>\n");
                            echo ("<p class=\"successcentr\">IMPORTANT: REMOVE YOUR DUMP FILE and BIGDUMP SCRIPT FROM SERVER NOW!</p>\n");
                            echo ("<p class=\"centr\">Thank you for using this tool! Please rate <a href=\"http://www.hotscripts.com/listing/bigdump/?RID=403\" target=\"_blank\">Bigdump at Hotscripts.com</a></p>\n");
                            echo ("<p class=\"centr\">You can send me some bucks or euros as appreciation via PayPal. Thank you!</p>\n");
                        }
                    }

                    exit;





































                    // DROP TABLE databases

                    /*
                    try
                    {
                        // SQLSTATE[HY000]: General error: 2006 MySQL server has gone away -> files to big
                        $query = $this->db->prepare(file_get_contents($file));
                        $query->execute();

                        # mysql --user=USERNAME --password=PASSWORD --host=HOST DB_NAME < /path/to/your/file.sql

                    }
                    catch (PDOException $e)
                    {
                        die("\n<p><strong>Error - ".__FUNCTION__."(".$file."):</strong> ".$e->getMessage()."</p>");
                    }
                    */
                }

            }

            //$db->query(str_replace('%#DB#%', $_POST['database'], file_get_contents('sql/dbvc.sql')))
            echo 'updated '.$database.' to '.$version;
        }
        else
        {
            return FALSE;
        }
    }

    public function updateVersion($version)
    {
        $values = array($version);
        try
        {
            $query = $this->db->prepare("INSERT INTO `".DB_NAME."`.`settings` (`id`, `version`, `executed_date`) VALUES (NULL, ?, CURRENT_TIMESTAMP);");
            if ($query->execute($values))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        catch (PDOException $e)
        {
            die("\n<p><strong>Error - ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
        }
    }

    public function getLatestVersion()
    {
        try
        {
            $query = $this->db->prepare("SELECT * FROM `".DB_NAME."`.`settings`ORDER BY `settings`.`executed_date` DESC LIMIT 1");
            if ($query->execute())
            {
                if ($settings = $query->fetchAll(PDO::FETCH_CLASS))
                {
                    return $settings[0]->version;
                }
                else
                {
                    return 'N/A';
                }
            }
            else
            {
                return 'N/A';
            }
        }
        catch (PDOException $e)
        {
            die("\n<p><strong>Error - ".__FUNCTION__."():</strong> ".$e->getMessage()."</p>");
        }
    }
}

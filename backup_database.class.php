<?php

class Backup_Database
{
    protected $host;
    protected $username;
    protected $passwd;
    protected $dbName;
    protected $charset;
    protected $db;

    protected function initializeDatabase()
    {
        $this->db = new PDO("mysql:host=$this->host;dbname=$this->dbName;charset=$this->charset", $this->username, $this->passwd );
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function __construct($host, $username, $passwd, $dbName, $charset = 'utf8')
    {
        $this->host     = $host;
        $this->username = $username;
        $this->passwd   = $passwd;
        $this->dbName   = $dbName;
        $this->charset  = $charset;

        $this->initializeDatabase();
    }

    protected function saveFile(&$sql, $table, $type, $outputDir = '.')
    {
        if (!$sql)
        {
            return FALSE;
        }

        try
        {
            $handle = fopen($outputDir.'/'.$this->dbName.'-'.$table.'-'.strtoupper($type).'.sql', 'w+');
            fwrite($handle, $sql);
            fclose($handle);
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            return FALSE;
        }

        return TRUE;
    }

    // ----------------------------------------------

    public function backupTables($tables = '*', $outputDir = '.')
    {
        try
        {
            // retrieve tables
            $tables = array();
            foreach ($this->db->query('SHOW TABLES') as $row)
            {
                $tables[] = $row[0];
            }

            // Iterate tables

            foreach ($tables as $table)
            {
                echo "Backing up ".$table." table...";

                /* table + database headers */

                $sql = 'CREATE DATABASE IF NOT EXISTS '.$this->dbName.";\n\n";
                $sql .= 'USE '.$this->dbName.";\n\n";

                $sql .= 'DROP TABLE IF EXISTS '.$table.';';

                /* dump of table */

                $result2 = $this->db->query('SHOW CREATE TABLE '.$table);
                $row2 = $result2->fetch();

                $sql.= "\n\n".$row2[1].";";

                $this->saveFile($sql, $table, 'table', $outputDir);


                /* dump of data */
                $result = $this->db->query('SELECT * FROM '.$table);
                $numFields = $result->columnCount();

                $sql = '';
                for ($i = 0; $i < $numFields; $i++)
                {
                    // while($row = mysql_fetch_row($result))

                    while ($row = $result->fetch())
                    {
                        $sql .= 'INSERT INTO '.$table.' VALUES(';
                        for ($j = 0; $j < $numFields; $j++)
                        {
                            $row[$j] = addslashes($row[$j]);
                            $row[$j] = preg_replace('/\n/' , "\\n" , $row[$j]);
                            if (isset($row[$j]))
                            {
                                $sql .= '"'.$row[$j].'"' ;
                            }
                            else
                            {
                                $sql.= '""';
                            }

                            if ($j < ($numFields-1))
                            {
                                $sql .= ',';
                            }
                        }

                        $sql.= ");\n\n";
                        #\r\n
                    }

                }
                $this->saveFile($sql, $table, 'data', $outputDir);


                echo " OK" . "<br />";
            }
        }
        catch (Exception $e)
        {
            var_dump($e->getMessage());
            return FALSE;
        }

        return TRUE;
    }
}
